/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional.examples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author appleman
 */
public class ReadingAFile {
    private static final String fileName = "/Users/appleman/Documents/"
                    + "NetbeansProjects/"
                    + "ExamplesFromFunctionalProgramming/duke.txt";
    
    public static void main(String[] args) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get(fileName)));
        System.out.println("content = " + content);
        System.out.println("content.length() = " + content.length());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional.examples;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author appleman
 */
public class LambdaExpressionAndFunctionalInterfacePredicate {

    public static void main(String args[]) {
        List<String> languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");
        
        System.out.println("Languages which starts with J :");
        filter(languages, (str) -> ((String)str).startsWith("J"));
//        filter(languages, (Object str) -> ((String)str).startsWith("J"));
         
        System.out.println("\nLanguages which ends with a ");
        filter(languages, (str) -> ((String)str).endsWith("a"));
        
        System.out.println("\nPrint all languages :");
        filter(languages, (str) -> true);
        
        System.out.println("\nPrint no language : ");
        filter(languages, (str) -> false);
        
        System.out.println("\nPrint language whose length greater than 4:");
        filter(languages, (str) -> ((String)str).length() > 4);
    }

    public static void filter(List<String> names, Predicate condition) {
        for (String name : names) {
            if (condition.test(name)) {
                System.out.println(name + " ");
            }
        }
    }

}

package functional.examples;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author appleman
 */
public class FindingElements {

    final static List<String> friends
            = Arrays.asList("Brian", "Nate", "Neal",
                    "Raju", "Sara", "Scott");

    public static void main(String[] args) {
        // Old approach
        final List<String> startsWithN = new ArrayList<String>();
        for (String name : friends) {
            if (name.startsWith("N")) {
                startsWithN.add(name);
            }
        }
        System.out.println("startsWithN  = " + startsWithN);

        // Use filter
        final List<String> startsWithN2 = friends.stream()
                .filter(name -> name.startsWith("N"))
                .collect(Collectors.toList());

        System.out.println("startsWithN2 = " + startsWithN2);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functional.examples;

/**
 *
 * @author appleman
 */
public class StreamingAString {

    public static void main(String[] args) {
        "hey duke".chars().forEach(c -> System.out.println((char)c));
        
        System.out.println("--------");  
        
        //The parallel version does not preserve the order 
        //      (and comes with additional overhead)
        "hey duke".chars().parallel().forEach(c -> System.out.println((char) c));
    }
}

package functional.examples;

import java.util.List;
import java.util.Arrays;
import java.util.function.Consumer;

public class FuncationalInteration {
	final static List<String> friends =
			Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
	
	final static String s1 = "The Java compiler also offers some lenience and"
			 				+ "can infer the types. Leaving out the type is "
							+ "convenient, requires less effort, and is less " 
							+ "noisy. Here’s the previous code without the type information.";
	final static String s2 = "The Java compiler treats single-parameter lambda " 
							+ "expressions as special: we can leave off the " 
							+ "parentheses around the parameter if the " 
							+ "parameter’s type is inferred.";
	final static String s3 = "used a method reference. Java lets us simply " 
							+ "replace the body of code with the method name of our choice.";
	
	public static void main(String[] args) {
		System.out.println("--traditional for loop");	
        for(int i = 0; i < friends.size(); i++) {
			System.out.println(friends.get(i));
		}



		System.out.println("\n-- foreach loop");	
		for(String name : friends) {
			System.out.println(name);
		}
		
		System.out.println("\n-- 1st functional using forEach"
						+ "\n\t method but anonymous method loop");	
		friends.forEach(new Consumer<String>() {
			public void accept(final String name) {
				System.out.println(name);
			}
		});
		
		System.out.println("\nreplacing the anonymous"
				+ "inner class with a lambda expression.");	
		friends.forEach((final String name) -> System.out.println(name));

		System.out.println("\n------ "+s1);	
		friends.forEach((name) -> System.out.println(name));  				

		System.out.println("\n------ "+s2);	
		friends.forEach(name -> System.out.println(name));
		
		System.out.println("\n------ "+s3);	
		friends.forEach(System.out::println);
		
	}
}
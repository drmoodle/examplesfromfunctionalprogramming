package functional.examples;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransformingAList {
    final static List<String> friends =
			Arrays.asList("Brian", "Nate", "Neal", 
                                      "Raju", "Sara", "Scott");	
    public static void main(String[] args) {
	
		List<String> uppercaseNames = new ArrayList<String>();
		for(String name : friends) { 
			uppercaseNames.add(name.toUpperCase());
		}
    		System.out.println(uppercaseNames);	

// Internal iterator forEach() method 
		final List<String> uppercaseNames2 = new ArrayList<String>();
		friends.forEach(name -> 
			uppercaseNames2.add(name.toUpperCase())
		);
		System.out.println(uppercaseNames2);
                
// The Stream’s map() method can map or transform a sequence of input 
// to a sequence of output—that fits quite well for the task at hand.
// collections/fpij/Transform.java
                friends.stream().map(name -> name.toUpperCase())
                        .forEach(name -> System.out.print(name + " "));
                System.out.println();
// The method stream() is available on all collections in JDK 8 and 
// it wraps the collection into an instance of Stream
    }	
}
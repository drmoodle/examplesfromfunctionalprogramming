/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remove.dupschar.array;

/*
format printout to see what everything is
*/
class RemoveDupsCharArray2 {

    public static void main(String[] args) {
        char[] str = {'a', 'b', 'a', 'c', 'k', 'a', 'j', 'b'};
        System.out.println("Before = " + (new String(str)));
        System.out.println("length of String:" + (new String(str)).length());

        for (int first = 0; first < str.length; first++) {
            for (int second = first + 1; second < str.length; second++) {
                if (str[first] == str[second]) {
                    str[second] = 8;
                }
                System.out.println("Array Length:" + str.length);
            }
            System.out.println("Array Length:" + str.length);
        }
        System.out.println("Array Length:" + str.length);
        System.out.println("After = " + (new String(str)) + "-------");
        System.out.println("length of String:" + (new String(str)).length());

        String str2 = new String(str);
        str2 = str2.toString();
        System.out.println("length of String2:" + str2.length());
        System.out.println("String2=" + str2 + "-----");

        for (int i = 0; i < str2.length(); i++) {
            char c = str2.charAt(i);
            int ii = c;
            System.out.println("char=" + c + ", int=" + ii);
        }

    }
}
